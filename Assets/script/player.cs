﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour {

	public Camera playerCam;
	public GameObject bulletPrefab;
	GameObject bulletPrefabs;
	
	public static int bulletCount;

	public Image healthBarImg;
	public static float helthMax=100f;
	public static float health;

	Animator anim;

	float helth;
	// Use this for initialization
	void Start () 
	{
		health=helthMax;
		anim=GetComponent<Animator>();
	}
	// Update is called once per frame
	void Update () 
	{
		healthBarImg.fillAmount=health/helthMax;
		if(healthBarImg.fillAmount==0)
		{
			GameManager.inst.GameFailCanvas.SetActive(true);
		}
		//print(healthBarImg.fillAmount);
		//if(Input.GetMouseButtonDown(0))
		if(LongClickButton.isBulletFire==true)
		 {
			
			bulletCount++;
			print("fire");
			bulletPrefabs=Instantiate(bulletPrefab);
			bulletPrefab.transform.position=playerCam.transform.position+ playerCam.transform.forward;
			bulletPrefab.transform.forward=playerCam.transform.forward;
			LongClickButton.isBulletFire=false;
		 }	
		//  if(LongClickButton.isStartAnim==true)
		//  {
		// 	anim.SetInteger("condition",1);
		//  }
		//  else
		//  {
		// 	 anim.SetInteger("condition",0);
		//  }
		 if(helth <0f)
		 {
			 enemy_2.isPlayerAlive=false;
			 Destroy(gameObject); 
		 }
	}

	public void playerFireRate()
	{
		anim.SetInteger("condition",0);
	}


}
