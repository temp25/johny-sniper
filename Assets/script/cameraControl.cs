﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraControl : MonoBehaviour {

	public float camSmoothingFactor =1;
	public float lookupMax=60;
	public float lookupMin=-60;
	private Quaternion camRotation;
	// Use this for initialization
	void Start () {
		camRotation=transform.localRotation;
	}
	
	// Update is called once per frame
	void Update () 
	{
		camRotation.x += Input.GetAxis("Mouse Y") *camSmoothingFactor *(-1);
		camRotation.y += Input.GetAxis("Mouse X") *camSmoothingFactor;
		
		camRotation.x=Mathf.Clamp(camRotation.x, lookupMin,lookupMax);
		camRotation.y=Mathf.Clamp(camRotation.y, lookupMin,lookupMax);

		transform.localRotation=Quaternion.Euler(camRotation.x, camRotation.y , camRotation.z);
		//transform.Rotate(new Vector3(0.1f,0f,0f));

			
	}
}
