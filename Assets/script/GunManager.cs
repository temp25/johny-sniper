﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunManager : MonoBehaviour
{

    public GameObject[] gunlist;
    public Text Damage_number,FireRateNumber,Stability_number,Magazine_number,ReloadSpeed_number,zoominOut_number;
    public Slider Damage_slide,Fire_slide,Stability_slide,magazine_slide,reload_slide,zoom_slider;
    // Start is called before the first frame update
    int a=1;
    void Start()
    {
        // if( PlayerPrefs.GetInt("FirestTImePlay",a)==1)
        // {

        // }

        // if(a==1)
        // {
        //     a++;
        //     PlayerPrefs.SetInt("FirestTImePlay",a);
        // }
        //PlayerPrefs.SetInt("FirestTImePlay",a);
        
       //PlayerPrefs.SetInt("FirestTImePlay",a);
         gunlist[0].SetActive(true);
         gunlist[1].SetActive(false);
         gunlist[2].SetActive(false);
         gunlist[3].SetActive(false);
        
         // Damage_slide.value=PlayerPrefs.SetFloat("Damage",);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AWMgunButton()
    {
        gunlist[0].SetActive(true);
        gunlist[1].SetActive(false);
        gunlist[2].SetActive(false);
        gunlist[3].SetActive(false);


        Damage_slide.value=0.30f;
        Fire_slide.value=0.20f;
        Stability_slide.value=0.55f;
        magazine_slide.value=0.20f;
        reload_slide.value=0.50f;
        zoom_slider.value=0.60f;

    }
    public void M_gunButton()
    {
        gunlist[0].SetActive(false);
        gunlist[1].SetActive(true);
        gunlist[2].SetActive(false);
        gunlist[3].SetActive(false);
        Damage_slide.value=0.30f;

        Fire_slide.value=0.30f;
        Stability_slide.value=0.65f;
        magazine_slide.value=0.40f;
        reload_slide.value=0.50f;
        zoom_slider.value=0.50f;
    }
    public void Ex_gunButton()
    {
        gunlist[0].SetActive(false);
        gunlist[1].SetActive(false);
        gunlist[2].SetActive(true);
        gunlist[3].SetActive(false);
        Damage_slide.value=0.60f;
        Fire_slide.value=0.30f;
        Stability_slide.value=0.35f;
        magazine_slide.value=0.20f;
        reload_slide.value=0.50f;
        zoom_slider.value=0.20f;
    }
    public void S_gunButton()
    {
         gunlist[0].SetActive(false);
         gunlist[1].SetActive(false);
         gunlist[2].SetActive(false);
         gunlist[3].SetActive(true);
         Damage_slide.value=0.24f;

        Fire_slide.value=0.40f;
        Stability_slide.value=0.45f;
        magazine_slide.value=0.50f;
        reload_slide.value=0.40f;
        zoom_slider.value=0.60f;
    }
}
