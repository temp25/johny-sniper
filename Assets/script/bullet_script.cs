﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_script : MonoBehaviour {

	public float speed=10f;
	public float lifeDuration=2f;

	private float lifeTime;
	// Use this for initialization
	void Start () 
	{
		lifeTime = lifeDuration;
	}
	// Update is called once per frame
	void Update () 
	{
		transform.position +=transform.forward * speed * Time.deltaTime;
		lifeTime -= Time.deltaTime;
		if(lifeTime<=0f)
		{
			Destroy(gameObject);
		}
	}
	public void OnTriggerEnter(Collider other)
    {
		 if(other.gameObject.CompareTag("Player"))
		 {
		 	print("  player collide");
		 	//isAlert=true;
		 	player.health -= 10f;
		 }


		//  if(other.gameObject.CompareTag("head"))
		//  {
		// 	GameManager.inst.headCount++;
		//  }
		
		  if(other.gameObject.CompareTag("Enemy"))
		  {
		  	print("  enemy collide");
		  	//isAlert=true;
		  	//player.health -= 10f;
		  }
		

    }
}
