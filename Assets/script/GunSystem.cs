﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSystem : MonoBehaviour 
{
	public int damage;
	public float timeBetweenShooting,spread,range,reloadTime,timeBetweenShots;
	public int magazineSize,bulletPerTap;
	public bool allowButtonHold;
	int bulletsLeft,bulletShot;

	bool shooting, readyToShoot, reloading;

	public Camera fpsCam;
	public Transform attackPoint;
	public RaycastHit rayhit;
	public LayerMask whatIsEnemy;

	private void Awake()
	{
		bulletsLeft=magazineSize;
		readyToShoot=true;
	}
	
	private void Update()
	{
		MyInput();
	}

	private void MyInput()
	{
		if(allowButtonHold)
		{
			shooting= Input.GetKey(KeyCode.Mouse0);
		}
		else
		{
			shooting=Input.GetKeyDown(KeyCode.Mouse0);
		} 
		if(Input.GetKeyDown(KeyCode.R) && bulletsLeft<magazineSize && !reloading)Reload();

		if(readyToShoot && shooting && !reloading && bulletsLeft>0)
		{
			bulletShot=bulletPerTap;
			Shoot();
		}

	}
	private void Shoot()
	{	
		readyToShoot =false;

		float x= Random.Range(-spread ,spread);
		float y=Random.Range(-spread,spread);

		Vector3 direction =fpsCam.transform.forward +new Vector3( x, y, 0);
		
		if(Physics.Raycast(fpsCam.transform.position,direction,out rayhit,range,whatIsEnemy))
		{
			Debug.Log(rayhit.collider.name);

			if(rayhit.collider.CompareTag("Enemy"))
			{
				Debug.Log("true");
				//rayhit.collider.GetComponent<ShootinAi>().TakeDamage(damage);
			}
		}

		bulletsLeft--;
		bulletShot--;
		Invoke("ResetShot",timeBetweenShooting);

		if(bulletShot > 0 && bulletsLeft >0 )
		Invoke("Shoot",timeBetweenShots);
	}
	
	
	private void ResetShot()
	{
		readyToShoot=true;
	}
	private void Reload()
	{
		reloading =true;
		Invoke("ReloaFinished",reloadTime);
	}

	private void ReloaFinished()
	{
		bulletsLeft=magazineSize;
		reloading=false;
	}

	
}
