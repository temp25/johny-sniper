﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemy_2 : MonoBehaviour {

	public static enemy_2 inst;
	public Transform player;
	public float playerDistance;
	public float rotationDamping;
	public static bool isPlayerAlive=true;

	public float enemyHealth=100.0f,maxHealth=100.0f;
	public GameObject healthBarUI;
	public Slider slider;

	//Animator anim;
	//GameObject bullet;
	float firerate, nextFire;

	public GameObject fire_pos;
	public GameObject bulletPrefab;
	GameObject bulletPrefabs;
	
	void Start () 
	{
		inst=this;
		//anim=GetComponent<Animator>();
		firerate=2f;
		nextFire=Time.time;
		//anim.SetInteger("condition",0);
	}
	// Update is called once per frame
	void Update () {
			slider.value=CalsulateValue();
			if(enemyHealth<maxHealth)
			{
				healthBarUI.SetActive(true);
			}
			if(enemyHealth<=0)
			{
				
				//Destroy(gameObject);
				print("enemy deth");
				isPlayerAlive=false;
				GameManager.inst.TotalEnemy--;
				//GameManager.inst.TotalEnemy = GameManager.inst.TotalEnemy-1;
				//anim.SetInteger("condi",0);
			}
			if(enemyHealth>maxHealth)
			{
				enemyHealth=maxHealth;
			}

		if(isPlayerAlive)
		{
			playerDistance = Vector3.Distance(player.position,transform.position);

			//if(playerDistance<15f)
			if(EnemyAlert.isAlert==true)
			{
				//print("look");
				lookAtPlayer();
			}
			//if(playerDistance <10f)
			if(EnemyAlert.isAlert==true)
			{
				//print("attack");
				attack();
			}
		}
	}

	float CalsulateValue()
	{
		return enemyHealth/maxHealth;
	}

	void lookAtPlayer()
	{
		Quaternion rotation = Quaternion.LookRotation(player.position - transform.position);
		transform.rotation=Quaternion.Slerp(transform.rotation,rotation,Time.deltaTime * rotationDamping );
	}
	void attack()
	{
		if(Time.time>nextFire)
		{
			//enemyPath.instance.anim.SetInteger("condi",1);
			//anim.SetInteger("condi",1);
			bulletPrefabs=Instantiate(bulletPrefab);
			bulletPrefab.transform.position=fire_pos.transform.position+ fire_pos.transform.forward;
			bulletPrefab.transform.forward=fire_pos.transform.forward;
			//Instantiate(bullet , transform.position , Quaternion.identity);
			nextFire=Time.time+firerate;	
		}

	}
	
}
