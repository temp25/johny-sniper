﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_Player : MonoBehaviour {

	public float speed=10f;
	public float lifeDuration=2f;

	private float lifeTime;
	public static bool isBlast=false;
	// Use this for initialization
	void Start () 
	{
		lifeTime = lifeDuration;
	}
	// Update is called once per frame
	void Update () 
	{
		transform.position +=transform.forward * speed * Time.deltaTime;
		lifeTime -= Time.deltaTime;
		if(lifeTime<=0f)
		{
			Destroy(gameObject);
		}
	}
	public void OnTriggerEnter(Collider col)
    {
		
		print(col.gameObject.tag + ".........");

		 if(col.gameObject.CompareTag("boiler"))
		   {
			   isBlast=true;
			  // Destroy(col.gameObject);
			   Destroy(GameObject.FindWithTag("boiler"));
		   }

		// if(other.gameObject.CompareTag("head"))
		// {
		// 	print("eneny colide");
		// }
			//print("  enemy collide");
		   if(col.gameObject.CompareTag("head"))
		   {
				enemy_2.inst.enemyHealth -= 100;
				//print("  enemy collide");
		   }
		   if(col.gameObject.CompareTag("body"))
		   {
			   enemy_2.inst.enemyHealth -= 50;
		   }
    }
}
