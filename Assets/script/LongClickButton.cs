﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class LongClickButton : MonoBehaviour,IPointerDownHandler,IPointerUpHandler {


	private bool pointDown;
	private float pointDownTimer;

	public float requireHoldTime;
	public UnityEvent OnLongClick;

	public static bool isBulletFire=false;

	public static bool isStartAnim=false;
	
	[SerializeField]
	private Image fillImage;

	public void OnPointerDown(PointerEventData eventData)
	{
		isStartAnim=true;
		//print("down call");
		pointDown=true;
		isBulletFire=false;
		Reset();
		Scope._insta.scoprOpen();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		//Reset();
		isBulletFire=true;
		Scope._insta.OnUnScope();
		isStartAnim=false;

		//print("ui call");
	}
	// Update is called once per frame
	void Update () {
		if(pointDown)
		{
			 pointDownTimer +=Time.deltaTime;
			 if(pointDownTimer > requireHoldTime)
			 {
			 	if(OnLongClick != null)
			 		OnLongClick.Invoke();
			 	Reset();
			 }
			
		}
	}
	private void Reset()
	{
		//print("reset callk");
		pointDown=false;
		pointDownTimer=0;
	//	fillImage.fillAmount=pointDownTimer / requireHoldTime;
	}
	// public void  OnPointerDown(PointerEventData eventData)
	// {
	// 	throw new System.NotImplementedException();
	// }
}
