﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swipeRotation : TouchLogic {

	public float rotatSpeed=10.0f;
	private float pitch =0.0f,yaw =0.0f;
	public int invertPitch=1;


	void OnTouchMovedAnywher()
	{
		pitch +=Input.GetTouch(0).deltaPosition.y * rotatSpeed * invertPitch *Time.deltaTime;
		yaw +=Input.GetTouch(0).deltaPosition.x * rotatSpeed* invertPitch *Time.deltaTime ;

		pitch = Mathf.Clamp(pitch,-80,80);
		this.transform.eulerAngles =new Vector3 (pitch,yaw,0.0f);
	}
}
