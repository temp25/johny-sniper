﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swiper : MonoBehaviour {

	private Touch intTouch=new Touch();
	public Camera cam;

	private float rotx=0f;
	private float roty=0f;
	private Vector3 origRot;

	public float rotSpeed=0.5f;
	public float dir =-1;
	
	// Use this for initialization
	void Start () 
	{
		origRot=cam.transform.eulerAngles;
		rotx=origRot.x;
		roty=origRot.y;
	}
	
	// Update is called once per frame
	void FixedUpdate() 
	{
		
		foreach(Touch touch in Input.touches)
		{
			print("check");
			if(touch.phase==TouchPhase.Began)
			{
				intTouch=touch;
			}
			else if(touch.phase==TouchPhase.Moved)
			{
				 float deltax = intTouch.position.x - touch.position.x;
				 float deltay = intTouch.position.y - touch.position.y;
				 rotx -= deltay * Time.deltaTime * rotSpeed * dir;
				 roty += deltax *Time.deltaTime * rotSpeed * dir;

				 Mathf.Clamp(rotx,-45f,45f);


				cam.transform.eulerAngles = new Vector3(rotx,roty,0f);
			}
			else if(touch.phase==TouchPhase.Ended)
			{
				 intTouch=new Touch();
			}

		}
		
	}
}
