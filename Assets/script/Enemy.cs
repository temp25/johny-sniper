﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {


	protected NavMeshAgent Agent;
	protected StateEnum State;
	protected target[] P_Target;
	protected target target_;

	protected float NextState;
	void Awake()
	{
		Agent = GetComponent<NavMeshAgent>();
		P_Target = FindObjectsOfType<target>();
		target_=P_Target[Random.Range(0,P_Target.Length)];
		State = StateEnum.Run;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Agent.updatePosition=false;
		Agent.updateRotation=false;
		Agent.updateUpAxis=false;

		NextState-=Time.deltaTime;

		switch (State)
		{
			case StateEnum.Run:
				if(Agent.desiredVelocity.magnitude < 0.1f)
				{
					State=StateEnum.Shoot;
					NextState=Random.Range(1f,7f);
				}
				break;
			case StateEnum.Shoot:
				
				if(NextState<0)
				{
					State=StateEnum.Run;
					target_=P_Target[Random.Range(0,P_Target.Length)];
					Agent.SetDestination(target_.transform.position);
				}
				break;
		}
		transform.position += Agent.desiredVelocity * Time.deltaTime;
	}
	public enum StateEnum
	{
		Run,
		Shoot
	}
}
