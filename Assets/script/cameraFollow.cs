﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {

	public float cameraMovement=120;
	public GameObject CameraFollowObj;
	Vector3 FollowPos;
	public float clampAngle;
	public float InputSensitivity=150f;
	public GameObject CameraObj;
	public GameObject PlayerObj;
	public float camDistanceXToPlayer;
	public float camDistanceYToPlayer;
	public float camDistanceZToPlayer;
	public float mouseX;
	public float mouseY;
	public float finalInputX;
	public float finalInputZ;
	public float smoothX;
	public float smoothY;
	private float rotY=0.0f;
	private float rotX=0.0f;
	
	
	// Use this for initialization
	void Start () 
	{
		Vector3 rot=transform.localRotation.eulerAngles;
		rotY =rot.y;
		rotX=rot.x;
		Cursor.lockState=CursorLockMode.Locked;
		Cursor.visible=false;

	}
	
	// Update is called once per frame
	void Update () 
	{
		float inputX=Input.GetAxis("RightStickHorizontal");
		float inputZ=Input.GetAxis("RightStickVertical");	
		mouseX= Input.GetAxis("Mouse X");
		mouseY=Input.GetAxis("Mouse Y");
		finalInputX = inputX + mouseX;
		finalInputZ = inputZ + mouseY;

		rotY += finalInputX*InputSensitivity * Time.deltaTime;
		rotX += finalInputZ* InputSensitivity*Time.deltaTime;

		rotX = Mathf.Clamp(rotX , -clampAngle , clampAngle);

		Quaternion localRotation=Quaternion.Euler(rotX,rotY,0.0f);
		transform.rotation=localRotation;
	}
	void LateUpdate()
	{
		CameraUpdate();
	}
	void CameraUpdate()
	{
		Transform target =CameraFollowObj.transform;
		float step = cameraMovement * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position,target.position,step);

	}
}
