﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyPath : MonoBehaviour {

	public static enemyPath instance;
	public GameObject[] waypoint;
	int current=0;
	float rotspeed;
	public float speed;
	float waPridict=1;
	public Animator anim;
	

	// Use this for initialization
	void Start () {
		anim=GetComponent<Animator>();
	}
	bool istrue=false;
	// Update is called once per frame
	void Update () {
		if(Vector3.Distance(waypoint[current].transform.position,transform.position)<waPridict)
		{
			anim.SetInteger("condi",0);
			current++;
			if(current >= waypoint.Length)
			{
				anim.SetInteger("condi",3);
				current=0;
			}
			if(EnemyAlert.isAlert==true)
			{
				anim.SetInteger("condi",1);
			}
			if(enemy_2.isPlayerAlive==false)
			{
				anim.SetInteger("condi",2);
			}
		}	
		if(istrue==false)
		{
			transform.position=Vector3.MoveTowards(transform.position,waypoint[current].transform.position,Time.deltaTime*speed);
		}
	}
}
