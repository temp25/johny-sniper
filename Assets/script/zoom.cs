﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zoom : MonoBehaviour {

	private Camera cam;
	private float startingFov;

	public float minFov,maxFov;
	public float zoomRate;
	private float currentFov;

	// Use this for initialization
	void Start ()
	{
		cam=GetComponent<Camera>();
		startingFov=cam.fieldOfView;
	}
	
	// Update is called once per frame
	private void Update ()
	 {
		currentFov=cam.fieldOfView;
		if(Input.GetKeyDown(KeyCode.P))
		{
			currentFov=startingFov;
		}
		if(Input.GetKey(KeyCode.I))
		{
			currentFov-=zoomRate;

		}
		if(Input.GetKey(KeyCode.O))
		{
			currentFov+=zoomRate;
		}
		currentFov=Mathf.Clamp(currentFov,minFov,maxFov);
		cam.fieldOfView = currentFov;
	}
	
}
