﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controll : MonoBehaviour {

	[SerializeField]
	float mouseSensitivity;
	[SerializeField]
	Transform player, playerArms;

	float xAxisClamp=0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		Cursor.lockState=CursorLockMode.Locked;
		RotateCamera();	
	}
	void RotateCamera()
	{
		float mouseX=Input.GetAxis("Mouse X");
		float mouseY = Input.GetAxis("Mouse Y");


		float rotAmountX =mouseX * mouseSensitivity;
		float rotAmountY = mouseY *mouseSensitivity;

		xAxisClamp -= rotAmountY;


		Vector3 rotPlayerArms=playerArms.transform.rotation.eulerAngles;
		Vector3 rotPlayer =player.transform.rotation.eulerAngles;

		rotPlayerArms.x -=rotAmountY;
		rotPlayerArms.z=0;
		rotPlayer.y += rotAmountY;

		if(xAxisClamp>90)
		{
			xAxisClamp=90;
			rotPlayerArms.x=90;
		}
		else if(xAxisClamp<-90)
		{
			xAxisClamp=-90;
			rotPlayerArms.x=-270;

		}

		playerArms.rotation =Quaternion.Euler(rotPlayerArms);
		player.rotation=Quaternion.Euler(rotPlayer);
	}
}
