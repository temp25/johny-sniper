﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosan : MonoBehaviour {

	public GameObject bomb;
	public float power=10f;
	public float radius =5.0f;
	public float upForce=1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(bomb==enabled)
		{
			Invoke("Detonate",5);
		}
	}
	void Detonate()
	{
		Debug.Log("call detonate function");
		Vector3 exposionPosition=bomb.transform.position;
		Collider[] colliders =Physics.OverlapSphere(exposionPosition,radius);
		foreach (Collider hit in colliders)
		{
			Rigidbody rgb=hit.GetComponent<Rigidbody>();
			if(rgb !=null)
			{
				rgb.AddExplosionForce(power,exposionPosition,radius,upForce,ForceMode.Impulse);
			}
		}
	}

}
