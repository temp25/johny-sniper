﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {


	public static GameManager inst; 
	// Use this for initialization
	public int TotalEnemy;
	private int TotalRemainEnemy;

	public int PlayerBulletReload;
	private int PlayerRemainBullet;
	public static int enemyCounter;

	 Slider DamageSlider;

	int Levelnumber=1;

	public Text TotalEnemyText;
	public Text RemainEnemyText;

	public Text LevelReward;
	public Text HeadshotReward;
	public int headCount;
	public Text CurrentLevel;

	public static bool isReload=false;
	 GameObject DamageButton;

	//public List<int> LevelsList;

	public GameObject GameCompletCanvas, GameFailCanvas,SettingCanvas;

	void Awake()
	{
		//PlayerPrefs.DeleteKey("Level");
		//Application.LoadLevel(PlayerPrefs.GetInt("Level"));
		CurrentLevel.text=PlayerPrefs.GetInt("Level",1).ToString(); 
		EnemyAlert.isAlert=false;
		//PlayerPrefs.GetInt("Level",1);
		 //Application.LoadLevel(PlayerPrefs.GetInt("Level",1));
//		 DamageSlider.value=PlayerPrefs.GetFloat("Damage");
		
	}

	void Start () 
	{
		inst = this;
		TotalEnemyText.text ="/0"+TotalEnemy.ToString();
		SettingCanvas.SetActive(false);
	}

	public void mute()
	{
		//AudioListener.pause = !AudioListener.pause;
		//PlayerPrefs.SetString
	}
	
	// Update is called once per frame
	void Update () 
	{
		RemainEnemyText.text=enemyCounter.ToString();
		if(PlayerBulletReload==player.bulletCount)
		{
			isReload=true;
			//print("Reload");
			StartCoroutine(waitforReload());
		}
		if(TotalEnemy==0)
		{
			LevelReward.text= (enemyCounter * 20).ToString();
			HeadshotReward.text=(headCount * 10).ToString();
			GameCompletCanvas.SetActive(true);
		}
		// if(enemy_2.isPlayerAlive==false)
		// {
		// 	TotalEnemy--;
		// }
	}
	 IEnumerator waitforReload()
	 {
		yield return new WaitForSeconds(5);
		player.bulletCount=0;
		isReload=false;
	 }
	 public void resetBtn()
	 {
		 Application.LoadLevel(Application.loadedLevel);
	 }

	int lvlCounrt;
	 public void NextButton()
	 {
		//Levelnumber++;
		Levelnumber=PlayerPrefs.GetInt("Level",Levelnumber);
		Levelnumber++;
		//Application.LoadLevel("Level"+Levelnumber);
		PlayerPrefs.SetInt("Level",Levelnumber);
		Application.LoadLevel("Level"+PlayerPrefs.GetInt("Level"));
		// PlayerPrefs.SetInt("Level",Levelnumber);
	 }
	 public void SettingBtn()
	 {
		 SettingCanvas.SetActive(true);
	 }
	 public void closeBtn()
	 {
		  SettingCanvas.SetActive(false);
	 }
	 float val;
	
	public void damageBtn()
	{
		 PlayerPrefs.SetFloat("Damage",(PlayerPrefs.GetFloat("Damage")+0.15f));
		 DamageSlider.value=PlayerPrefs.GetInt("Damage");
		 DamageButton.SetActive(false);
	}
}
